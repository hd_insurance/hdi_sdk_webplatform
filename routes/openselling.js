const Router = require("express").Router();
const { celebrate, Joi, errors } = require("celebrate");
const Controller = require("../controller/OpenSelling");

Router.post("/api/get-define", Controller.getDefine);
Router.post("/api/get-detail", Controller.getDetail);

Router.post("/api/bhsk/calc/fee", Controller.calculatorFee);

Router.post("/api/upload", Controller.uploadDocument);

Router.post("/api/upload", Controller.uploadDocument);
Router.post("/api/ntn/calc/fee", Controller.calcFeeNhaTuNhan);

Router.post(
  "/api/createOrder/:action/:product_code/:channel/:category/:username",
  Controller.createOder
);

Router.post("/api/view-certificate", Controller.getGYC);

Router.use(errors());

module.exports = Router;
