// import {Route} from "react-router-dom";
const path = require('path');
const Router = require('express').Router();
const commonRoutes = require('./common');
const SDKController = require('../controller/sdk.js');
const TraCuuRouter = require('./tracuu.js');
const OpenSellingRouter = require('./openselling.js');

Router.use(commonRoutes);
Router.use(TraCuuRouter);
Router.use(OpenSellingRouter);

Router.get('/source/:hash/integrate.js', SDKController.getSDK)

Router.get('/f/:hash', SDKController.formLayout);

Router.get('/testIntegrated', function (req, res) {
    res.sendFile(path.join(__dirname + '/testIntegrated.html'))
});

/**
 * Running flutter in NodeJS demo
 * Build flutter web, copy to folder public-flutterDmo
 * Iframe:  https://htmldom.dev/communication-between-an-iframe-and-its-parent-window/
 Router.get('/flutter', function (req, res) {
     res.sendFile(path.join(__dirname, '../public-flutterDemo/index.html'))
    });    
*/


module.exports = Router;