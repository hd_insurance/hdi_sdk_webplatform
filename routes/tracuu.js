const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/tracuu');


Router.post('/api/tracuu-gcn', Controller.getGCN)
Router.post('/api/document/tracuu-gcn', Controller.getGCNDocument)
Router.post('/api/search-gcn', Controller.searchGCN);
Router.post('/api/define-type', Controller.getDefineType);
Router.post('/api/print-gcn', Controller.getPrintGCN);
Router.post('/api/billing-request/:org_code', Controller.billingRequest);
Router.post('/api/create-token/:certificate_no', Controller.createTokenConfirm);
Router.post('/api/confirm-token/:certificate_no/:token', Controller.confirmTokenEmail);



Router.use(errors());
module.exports = Router;