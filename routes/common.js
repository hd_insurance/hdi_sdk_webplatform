const Router = require('express').Router();
const { celebrate, Joi, errors } = require('celebrate');
const Controller = require('../controller/common');


Router.get('/api/location/:utype/:code', Controller.getLocation)


Router.use(errors());
module.exports = Router;