require('dotenv').config();
const path = require('path');
const config = require('./config');
const express = require('express');
const routes = require('./routes');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors());
app.options("*", cors());
app.use(express.static('public'))
// app.use('/flutter',express.static(path.join(__dirname, 'public-flutter/web_copy')));
app.set('views', __dirname + '/resources');
app.set('view engine', 'ejs');
app.use(routes);

app.listen(config.app.port, () => {
	console.info(`SDK SERVER START AT PORT ${config.app.port}`)
});
