class HDISdk {
  constructor(config) {
    this.host = "https://beta-sdk.hdinsurance.com.vn";
    // this.host = "http://localhost:2021";

  	// this.host = "https://sdk.hdinsurance.com.vn";
    this.classId = config.classId;
    this.init = this.init.bind(this);
    this.open = this.open.bind(this);
    this.sdk_params = config.sdk_params || {};
    window.sdk_params = config.sdk_params || {};
    // window.closeForm = this.closeForm.bind(this); 
    $('head').append('<link rel="stylesheet" href="'+this.host+'/resource/css/app.css"/>');
  }

  init(callback){
  	const classname = "#"+this.classId
    const pathJS = "/resource/static/js/"
    const pathCSS = "/resource/static/css/"
	  $(""+classname).append( $( '<div id="root"></div>' ) );
    $('foot').append('<script src="'+this.host+pathJS+'2.4ee0fbdf.chunk.js"></script>');
    $('foot').append('<script src="'+this.host+pathJS+'main.6d6bd5be.chunk.js"></script>');
	// $('head').append('<link rel="stylesheet" href="'+this.host+pathCSS+'2.ae9f1eb3.chunk.css"/>');
	$('head').append('<link rel="stylesheet" href="'+this.host+pathCSS+'main.1642a0b7.chunk.css"/>');
	callback({
		sucess: true,
    info:{}
	})
  }

  open(sdk_params){
    window.sdk_params = sdk_params;
    // window.openForm(sdk_params);
    openForm(sdk_params);
  }

  onFormsubmit(){
  	return {}
  }
  onFormClose(){
  	return {}
  }
  closeForm(){

  }

}
function openForm (sdk_params){
  return sdk_params;
}

!function(e){function r(r){for(var n,i,a=r[0],l=r[1],c=r[2],s=0,p=[];s<a.length;s++)i=a[s],Object.prototype.hasOwnProperty.call(o,i)&&o[i]&&p.push(o[i][0]),o[i]=0;for(n in l)Object.prototype.hasOwnProperty.call(l,n)&&(e[n]=l[n]);for(f&&f(r);p.length;)p.shift()();return u.push.apply(u,c||[]),t()}function t(){for(var e,r=0;r<u.length;r++){for(var t=u[r],n=!0,a=1;a<t.length;a++){var l=t[a];0!==o[l]&&(n=!1)}n&&(u.splice(r--,1),e=i(i.s=t[0]))}return e}var n={},o={1:0},u=[];function i(r){if(n[r])return n[r].exports;var t=n[r]={i:r,l:!1,exports:{}};return e[r].call(t.exports,t,t.exports,i),t.l=!0,t.exports}i.e=function(e){var r=[],t=o[e];if(0!==t)if(t)r.push(t[2]);else{var n=new Promise((function(r,n){t=o[e]=[r,n]}));r.push(t[2]=n);var u,a=document.createElement("script");a.charset="utf-8",a.timeout=120,i.nc&&a.setAttribute("nonce",i.nc),a.src=function(e){return i.p+"static/js/"+({}[e]||e)+"."+{3:"36451324"}[e]+".chunk.js"}(e);var l=new Error;u=function(r){a.onerror=a.onload=null,clearTimeout(c);var t=o[e];if(0!==t){if(t){var n=r&&("load"===r.type?"missing":r.type),u=r&&r.target&&r.target.src;l.message="Loading chunk "+e+" failed.\n("+n+": "+u+")",l.name="ChunkLoadError",l.type=n,l.request=u,t[1](l)}o[e]=void 0}};var c=setTimeout((function(){u({type:"timeout",target:a})}),12e4);a.onerror=a.onload=u,document.head.appendChild(a)}return Promise.all(r)},i.m=e,i.c=n,i.d=function(e,r,t){i.o(e,r)||Object.defineProperty(e,r,{enumerable:!0,get:t})},i.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},i.t=function(e,r){if(1&r&&(e=i(e)),8&r)return e;if(4&r&&"object"==typeof e&&e&&e.__esModule)return e;var t=Object.create(null);if(i.r(t),Object.defineProperty(t,"default",{enumerable:!0,value:e}),2&r&&"string"!=typeof e)for(var n in e)i.d(t,n,function(r){return e[r]}.bind(null,n));return t},i.n=function(e){var r=e&&e.__esModule?function(){return e.default}:function(){return e};return i.d(r,"a",r),r},i.o=function(e,r){return Object.prototype.hasOwnProperty.call(e,r)},i.p="/",i.oe=function(e){throw console.error(e),e};var a=this["webpackJsonpselling-b2b"]=this["webpackJsonpselling-b2b"]||[],l=a.push.bind(a);a.push=r,a=a.slice();for(var c=0;c<a.length;c++)r(a[c]);var f=l;t()}([])
