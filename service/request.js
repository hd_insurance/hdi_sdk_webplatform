var needle = require('needle');
const https = require('https');
var CryptoJS = require("crypto-js");
const config = require('../config');

class Request {
	constructor() {
		this.login = this.login.bind(this)
		this.getOption = this.getOption.bind(this)
		this.signRequest = this.signRequest.bind(this)
		this.send = this.send.bind(this)
	}
	send(url, data, actioncode, isFirstload=true){
		return new Promise(async (resolved, rejected)=>{
			const signature = this.signRequest(actioncode, data)
			const params = {
					Device: {
						DeviceId: "",
						DeviceCode: "",
						Device_name: "",
						IpPrivate: config.auth.ipPrivate,
						IpPublic: "",
						X: "",
						Y: "",
						province: "",
						district: "",
						wards: "",
						address: "",
						environment: "LIVE",
						browser: "",
						DeviceEnvironment: "WEB"
					},
					Action: {
						ParentCode: config.auth.parent_code,
				        UserName: config.auth.username,
				        Secret: config.auth.secret,
						ActionCode: actioncode
					},
					Data: data,
					Signature: signature
				}
			if(config.auth.token==null){
				const loginresult = await this.login()
				if(loginresult){
					config.auth.token = loginresult
				}else{
					return rejected("Login failed")
				}
			}
			const options = this.getOption(config.auth.token)
			const that = this
			needle.request('post', url, params, options, async function(err, resp) {
			  if (!err && resp.statusCode == 200){
			  	if(resp.body.success||resp.body.Success){
			  		return resolved(resp.body)
			  	}else{
			  		console.log("HDI-Request Error x : ",err)
			  		return rejected(resp.body)
			  	}
			  }else{
			  	if(resp.body.Error == 'ERROR_2004'){
			  		if(isFirstload){
			  			const loginresult = await that.login()
						if(loginresult){
							config.auth.token = loginresult
							const options = that.getOption(loginresult)
							needle.request('post', url, params, options, async function(err, resp) {
							  if (!err && resp.statusCode == 200){
							  	if(resp.body.success||resp.body.Success){
							  		return resolved(resp.body)
							  	}else{
							  		console.log("HDI-Request Error x : ",err)
							  		return rejected(resp.body)
							  	}
							  }else{
							  	console.log("HDI-Request Error x3 : ",err)
							  	return rejected(resp.body)
							  }
							    
							});
						}else{
							return rejected("Login failed")
						}
			  		}else{
			  			console.log("HDI-Request Error x4: ",err, resp)
			  			return rejected(false)
			  		}
			  	}else{
			  		console.log("HDI-Request Error x2: ",err, resp)
			  		return rejected(false)
			  	}
			  	
			  }
			    
			});



		})
		
	}
	signRequest(actionCode, data){
		const constantsText = "HDI";
		const str_data = JSON.stringify(data)
		const str_md5 = CryptoJS.MD5(str_data)
		const raw = constantsText + config.auth.deviceCode + config.auth.ipPrivate + config.auth.deviceEnvironment + config.auth.username + config.auth.secret + actionCode + config.auth.parent_code + str_md5.toString(CryptoJS.enc.Hex).toUpperCase() + constantsText;
		const encryptedMD5 = CryptoJS.MD5(raw)
		const sha256 = CryptoJS.HmacSHA256(encryptedMD5.toString(CryptoJS.enc.Hex).toUpperCase(), config.auth.CLIENT_ID)
		return sha256.toString(CryptoJS.enc.Hex).toUpperCase()
	}
	getOption(token){
		const agent = new https.Agent({  
	        rejectUnauthorized: false
	      }); 
	    var rm = {
	  		json:true,
			headers: {
			 'Access-Control-Allow-Headers': 'Content-Type',
			 'Access-Control-Allow-Methods': 'POST',
			 "Content-Type": "application/json",
			 'Access-Control-Allow-Origin': '*',
			 "Token": ""+token  }
			}   

		if(process.env.USE_SSL == "true"){
			rm.agent = agent
		}
	  	return rm
	}

	login(){
		return new Promise(async (resolved, rejected)=>{
			try{
				// console.log(new Date().getTime() - config.auth.lastLogin)
				const params = {
						Device: {
							DeviceId: "",
							DeviceCode: "",
							Device_name: "",
							IpPrivate: "123",
							IpPublic: "",
							X: "",
							Y: "",
							province: "",
							district: "",
							wards: "",
							address: "",
							environment: config.app.ENVIRONMENT,
							browser: "",
							DeviceEnvironment: "WEB"
						},
						Action: {
							ParentCode: config.auth.parent_code,
							UserName: config.auth.username,
							Secret: config.auth.secret,
							ActionCode: "HDI_API_LOGIN"
						},
						Data: {
							UserName: config.auth.username,
							Password: config.auth.password
						},
						Signature: ""
					}

					const options = this.getOption("xxxx")

					console.log(params.Action)

					needle.request('post', config.domain1+'/api/login', params, options, function(err, resp) {
					  if (!err && resp.statusCode == 200){
					  	if(resp.body.Success){
					  		config.auth.token = resp.body.Token
					  		config.auth.lastLogin = new Date().getTime()
					  		config.auth.expireIn = resp.body.Expries
					  		return resolved(resp.body.Token)
					  	}else{
					  		return resolved(false)
					  	}
					  }else{
					  	console.log("LoginError", resp.body.validation)
					  	console.log("LoginError", err)
					  	return resolved(false)
					  }
					    
					});
			}catch(e){
				console.log("Login Error  " , e)
				return resolved(false)
			}
		})
	}
}
module.exports = Request