const envVariables = {
  env: process.env.NODE_ENV,
  domain1: process.env.DM1,
  domain2: process.env.DM2,
  app: {
    secretKey: process.env.APP_SECRET_KEY || '',
    port: process.env.SERVER_PORT || 2021,
    prefix: 'NJS_',
    login_timeout: 5,
    ENVIRONMENT: "LIVE",
    rate_limiter: {
      windowMs: 15 * 60 * 1000,
      max: 300 // limit each IP to 100 requests per windowMs
    }
  },
  auth:{
      deviceCode: "",
      deviceEnvironment: "WEB",
      ipPrivate: "123",
      parent_code: process.env.PTN_CODE,
      username: process.env.USR_NAME,
      secret: process.env.SECRET,
      password: process.env.PASSWORD,
      CLIENT_ID: process.env.CLIENT_ID,
      token: null,
      lastLogin: 0,
      expireIn: 0
  }
}
module.exports = envVariables;