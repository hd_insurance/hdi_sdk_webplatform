const fs = require('fs');
const path = require('path');
var JavaScriptObfuscator = require('javascript-obfuscator');


this.getSDK = (req, res, next)=>{
	res.type('.js');
	try {
	    var data = fs.readFileSync('./resources/sdk.js', 'utf8');
	    var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
	    res.send(obfuscationResult.getObfuscatedCode())
	} catch(e) {
	    console.log('Error:', e.stack);
	    res.send("")
	}

	// res.render("sdk", {host: "https://beta-sdk.hdinsurance.com.vn", jsfiles: jsfiles, cssfiles:cssfiles});
}

this.formLayout = (req, res, next)=>{
	res.render("sdkview")
}


module.exports = {
  getSDK: this.getSDK,
  formLayout: this.formLayout
}
