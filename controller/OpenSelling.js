const HDIRequest = require("../service/request");
const config = require("../config");

const getDefine = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const defineParams = {
      A1: req.body.org_code, // ORG_CODE 
      A2: req.body.product_code, // PRODUCT_CODE
      A3: "",
      A4: req.body.channel,
      A5: "VN",
      A6: "",
      A7: "",
      A8: "",
      A9: "",
      A10: "",
      A11: "",
      A12: "",
      A13: "",
      A14: "",
      A15: "",
      A15: "",
    };
    console.log('======> get define: ', defineParams);
    const response = await rq.send(
      config.domain1 + "/open-api",
      {
        P_PROJECT: "LANDING_SDK",
        P_USERNAME: "X",
        P_O_BUSINESS: JSON.stringify(defineParams),
      },
      "APIY6GZ0T7"
    );
    if (response.success) {
      if (response.data) {
        res.send(response.data);
      }
    } else {
      console.log("GET PACKAGE ERROR 1");
      res.send([]);
    }
  } catch (e) {
    console.log("GET PACKAGE ERROR ", e);
    res.send([]);
  }
};

const calculatorFee = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    let paramsCalcFee = {
      A1: req.body.a1 || "",
      A2: req.body.a2 || "",
      A3: req.body.a3 || "",
      A4: req.body.a4 || "",
      A5: req.body.a5 || "",
      A6: req.body.a6 || "",
      A7: req.body.a7 || "",
      A8: req.body.a8 || "",
      A9: req.body.a9 || "",
      A10: req.body.a10 || "",
      A11: req.body.a11 || "",
      A12: req.body.a12 || "",
      A13: req.body.a13 || "",
      A14: req.body.a14 || "",
      A15: req.body.a15 || "",
      A16: req.body.a16 || "",
    };
    const params = {
      p_project: "",
      p_username: "",
      p_o_business: JSON.stringify(paramsCalcFee),
    };
    const response = await rq.send(
      config.domain1 + "/open-api",
      params,
      "APIXE6WGR9"
    );
    if (response.data) {
      res.send(response.data);
    } else {
      res.send([]);
    }
  } catch (e) {
    console.log("CALCULATOR FEE ERROR ", e);
    res.send([]);
  }
};

const calcFeeNhaTuNhan = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const data = req.body;
    console.log("=====> data", data);
    const response = await rq.send(
      config.domain2 + "/OpenApi/insur/calculate/v3/fees",
      data,
      "APIZTPI8X1"
    );
    res.send(response);
  } catch (err) {
    console.log("error nè", err);
  }
};

const uploadDocument = async (req, res) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.sendFile(config.domain1 + "/upload", req.files);
    res.send(response);
  } catch (e) {
    console.log(e);
    res.send({ data: [] });
  }
};

const getDetail = async (req, res) => {
  try {
    const Data = req.body;
    console.log(Data);
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain2 + "/OpenApi/hdi/v1/product/detail",
      Data,
      "API4CQ0Z1U"
    );
    if (response.Success) {
      // console.log(response)
      res.send(response);
    } else {
      console.log("CREATE ORDER ERROR 1");
      res.send([]);
    }
  } catch (error) {
    console.log(error);
    res.send({ data: [] });
  }
};

const createOder = async (req, res) => {
  try {
    const { SELLER, BUYER, HEALTH_INSUR, HOUSE_INSUR, PAY_INFO, FILE, BILL_INFO, URL } =
      req.body;
    const rq = new HDIRequest();

    let data = {
      CHANNEL: req.params.channel,
      USERNAME: req.params.username,
      PRODUCT_CODE: req.params.product_code,
      CATEGORY: req.params.category,
      ACTION: req.params.action,
      SELLER: SELLER,
      BUYER: BUYER,
      HEALTH_INSUR: HEALTH_INSUR,
      PAY_INFO: PAY_INFO,
      FILE: FILE,
      BILL_INFO: BILL_INFO,
    };
    if(req.params.category == 'TS10'){
      data = {
        CHANNEL: req.params.channel,
        USERNAME: req.params.username,
        PRODUCT_CODE: req.params.product_code,
        CATEGORY: req.params.category,
        ACTION: req.params.action,
        SELLER: SELLER,
        BUYER: BUYER,
        HOUSE_INSUR: HOUSE_INSUR,
        PAY_INFO: PAY_INFO,
        FILE: FILE || null,
        BILL_INFO: BILL_INFO || null,
    }
    }
    console.log(URL);
    const response = await rq.send(config.domain2 + URL, data, "APIK70NHXK");

    if (response.Success) {
      // console.log(response)
      res.send(response);
    } else {
      console.log("CREATE ORDER ERROR 1");
      res.send(response);
    }
  } catch (e) {
    console.log("CREATE ORDER ERROR ", e);
    res.send(e);
  }
};

const getGYC = async (req, res) => {
  try {
    const data = req.body;
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain2 + "/hdi/service/viewCertificate",
      data,
      "GET_TEMPLATE"
    );

    if (response.Success) {
      // console.log(response)
      res.send(response);
    } else {
      console.log("CREATE ORDER ERROR 1");
      res.send(response);
    }
  } catch (error) {
    console.log("err while get GYC BH", error);
    res.send(error);
  }
};

module.exports = {
  getDefine: getDefine,
  calculatorFee: calculatorFee,
  calcFeeNhaTuNhan: calcFeeNhaTuNhan,
  uploadDocument: uploadDocument,
  createOder: createOder,
  getDetail: getDetail,
  getGYC: getGYC,
};
