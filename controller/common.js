const HDIRequest = require('../service/request');
const config = require('../config');

const getLocation = async (req, res, next)=>{
	try{
		const rq = new HDIRequest()
		const response = await rq.send(config.domain1+'/open-api', {p_utype: req.params.utype, p_code:req.params.code}, "GET_LOCATION")
		res.send(response)
	}catch(e){
		console.log(e)
		res.send({"success":false, data:[[]]})
	}
	
}


module.exports = {
	getLocation: getLocation
}
