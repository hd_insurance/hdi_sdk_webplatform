const HDIRequest = require("../service/request");
const config = require("../config");

const getGCN = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain2 + "/hdi/service/findCer",
      {
        INS_CONTRACT: req.body.no || "",
        FILE_CODE: req.body.f || "",
        FILE_TYPE: "FILE",
      },
      "HDI_GET_SIGN"
    );
    if (response.Success) {
      console.log("====>", response.Data);
      res.send(response.Data);
    } else {
      res.send({ success: false, data: {} });
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: {} });
  }
};

const getGCNDocument = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain2 + "/hdi/service/findCer",
      {
        INS_CONTRACT: req.body.no || "",
        FILE_CODE: req.body.f || "",
        FILE_TYPE: "FILE",
      },
      "HDI_GET_SIGN"
    );
    if (response.Success) {
      res.send(response.Data);
    } else {
      res.send({ success: false, data: {} });
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: {} });
  }
};

const searchGCN = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    let searchParams;
    if (!req.body.fileId) {
      searchParams = {
        A1: req.body.a1 || "",
        A2: req.body.a2 || "",
        A3: req.body.a3 || "",
        A4: req.body.a4 || "",
        A5: req.body.a5 || "",
        A6: req.body.a6 || "",
        A7: req.body.a7 || "",
        A8: req.body.a8 || "VN",
        A9: req.body.a9 || "",
        A10: req.body.a10 || "",
        A11: req.body.a11 || "",
        A12: req.body.a12 || "",
        A13: req.body.a13 || "",
        A14: req.body.a14 || "",
        A15: req.body.a15 || "",
        A16: req.body.a16 || "",
      };
    } else {
      searchParams = {
        A1: req.body.fileId || "",
        A2: req.body.a2 || "",
        A3: "FILE",
        A4: req.body.a4 || "",
        A5: req.body.a5 || "",
        A6: req.body.a6 || "",
        A7: req.body.a7 || "",
        A8: req.body.a8 || "VN",
        A9: req.body.a9 || "",
        A10: req.body.a10 || "",
        A11: req.body.a11 || "",
        A12: req.body.a12 || "",
        A13: req.body.a13 || "",
        A14: req.body.a14 || "",
        A15: req.body.a15 || "",
        A16: req.body.a16 || "",
      };
    }
    const params = {
      p_project: "",
      p_username: "",
      p_o_business: JSON.stringify(searchParams),
    };
    const response = await rq.send(
      config.domain2 + "/OpenApi/GetCerInfo",
      params,
      "API8ORT2VM"
    );
    // console.log(response.Data);
    if (response.Success && response.Data.length > 0) {
      return res.send({ data: response.Data });
    }
    return res.send({ success: false, data: [] });
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: [] });
  }
};

const getDefineType = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const params = {
      p_channel: "",
      p_username: "",
      p_keyword: "LANDING_SEARCH",
      p_page: 0,
      p_is_active: "ALL",
    };
    const response = await rq.send(
      config.domain1 + "/open-api",
      params,
      "HDI_37"
    );
    if (response.success && response.data.length > 0) {
      return res.send({ data: response.data });
    }
    return res.send({ success: false, data: [] });
  } catch (err) {
    console.log(err);
  }
};

const getPrintGCN = async (req, res, next) => {
  try {
    const rq = new HDIRequest();
    const response = await rq.send(
      config.domain2 + "/hdi/service/addTextGCN",
      {
        FILE_CODE: req.body.fileId,
        CER_ID: req.body.gcnId,
      },
      "HDI_EMAIL_JSON"
    );
    if (response.Success) {
      return res.send({success: true, data: response.Data });
    } else {
      res.send({ success: false, data: {} });
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, data: {} });
  }
};


const billingRequest = async (req, res) =>{
  try {
    const rq = new HDIRequest();
    const data = {
      p_project: "",
      p_username: "WEB",
      p_org_code: req.params.org_code,
      p_o_business: JSON.stringify(req.body)
    }
    const response = await rq.send(config.domain1+'/open-api', data, "APIN2I2D2Y")
    res.send(response)
  }catch (e) {
    console.log(e)
    res.send({success:false})
  }
}

const createTokenConfirm = async (req, res) =>{
  try {
    const rq = new HDIRequest();
    const data = {
      TYPE_CODE: "HOA_DON",
			PARAM_VALUE: req.params.certificate_no,
			TYPE_SEND: "EMAIL",
			LENG: "6"
    }

    const response = await rq.send(config.domain2 + '/hdi/service/getTokenVerify', data, "APILO3NM1O")
    
    res.send(response)
  }catch (e) {
    console.log(e)
    res.send(e)
  }
}

const confirmTokenEmail = async (req, res) =>{
  try {
    const rq = new HDIRequest();
    const data = {
      TYPE_CODE: "HOA_DON",
			PARAM_VALUE: req.params.certificate_no,
			TYPE_SEND: "EMAIL",
			TOKEN: req.params.token
    }

    const response = await rq.send(config.domain2 + '/hdi/service/verifyToken', data, "APIU640R0R")
    
    res.send(response)
  }catch (e) {
    console.log(e)
    res.send(e)
  }
}

module.exports = {
  getGCN: getGCN,
  getGCNDocument: getGCNDocument,
  searchGCN: searchGCN,
  getDefineType: getDefineType,
  getPrintGCN: getPrintGCN,
  billingRequest: billingRequest,
  createTokenConfirm: createTokenConfirm,
  confirmTokenEmail: confirmTokenEmail,
};
